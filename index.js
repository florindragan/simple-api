const {faker} = require('@faker-js/faker');

module.exports = () => {
    const data = {users: []}
    // Create 1000 users
    for (let i = 0; i < 1000; i++) {
        const name = {
            firstName: faker.person.firstName(),
            lastName: faker.person.lastName()
        }
        data.users.push({
            id: faker.string.uuid(),
            name: `${name.firstName} ${name.lastName}`,
            email: faker.internet.email({firstName: name.firstName, lastName: name.lastName})
        });
    }
    return data;
};
